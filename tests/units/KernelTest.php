<?php

namespace Test\Unit;

use Illuminate\View\View;
use Kernel\_Kernel;
use \PHPUnit\Framework\TestCase;

class KernelTest extends TestCase
{
    protected $envEngine;
    protected $dataStorage;
    protected $errorHandler;
    protected $dbConnection;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->envEngine = new \Kernel\EnvEngine();
        $this->dataStorage = new \Kernel\ArrayDataStorage();
        $this->errorHandler = new \Kernel\ErrorHandler();
        $this->dbConnection = new \Kernel\MysqlDbConnection($this->envEngine, $this->errorHandler);
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_SERVER['HTTP_HOST'] = 'http';
        $_SERVER['REQUEST_URI'] = '/';
    }

    public function testConstruction()
    {
        #Arrange

        #Act
        $kernel = new _Kernel($this->dbConnection, $this->envEngine, $this->dataStorage);

        #Assert
        $this->assertInstanceOf(_Kernel::class, $kernel);
    }

}